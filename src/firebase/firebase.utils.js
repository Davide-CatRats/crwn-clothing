import firebase from "firebase/app";
import "firebase/firestore";
import "firebase/auth";

const config = {
  apiKey: "AIzaSyDQaYSuGoZW72iaz9HKWfhDjd1YBugYZjo",
  authDomain: "new-react-app-project.firebaseapp.com",
  databaseURL: "https://new-react-app-project.firebaseio.com",
  projectId: "new-react-app-project",
  storageBucket: "new-react-app-project.appspot.com",
  messagingSenderId: "901798554266",
  appId: "1:901798554266:web:cf3fc1333c392981598d23",
  measurementId: "G-0S5RQHZ667"
};

export const createUserProfileDocument = async (userAuth, additionalData) => {
  if (!userAuth) return;

  const userRef = firestore.doc(`users/${userAuth.uid}`);

  const snapShot = await userRef.get();

  if (!snapShot.exists) {
    const { displayName, email } = userAuth;
    const createdAt = new Date();

    try {
      await userRef.set({
        displayName,
        email,
        createdAt,
        ...additionalData
      });
    } catch (error) {
      console.log("error creating user", error.message);
    }
  }

  return userRef;
};

firebase.initializeApp(config);

export const auth = firebase.auth();
export const firestore = firebase.firestore();

const provider = new firebase.auth.GoogleAuthProvider();
provider.setCustomParameters({ prompt: "select_account" });
export const signInWithGoogle = () => auth.signInWithPopup(provider);

export default firebase;
